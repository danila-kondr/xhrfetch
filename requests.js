// vim: tw=80 ts=2 sw=2 expandtab

const XHR   = 1;
const FETCH = 2;
const SMALL = 1;
const BIG   = 2;

const finishedSeries = document.getElementById("finished-series");

/**
 * Results data model:
 *
 * resultsData.get(<Series ID>) = {
 *   source: "small.html" or "big.html",
 *   iterations: number,
 *   experiments: new Map()
 * };
 *
 * resultsData.get(<Series ID>).experiments.get(<Experiment ID>) = {
 *   type: 1 = xhr or 2 = fetch,
 *   expStart: ISO 8601 string,
 *   requests: new Map();
 * };
 *
 * resultsData.get(<Series ID>).experiments.get(<Experiment ID>).requests = {
 *   request1: number,
 *   request2: number,
 *   ...
 *   requestN: number
 * } (AS MAP)
 *
 * Every series consist of two experiments: for XMLHttpRequest and for Fetch
 * API.
 *
 * Every experiment consist of specified number of request iterations which are
 * executed one by one. Duration is received from Performance API methods and
 * recorded into the "requests" field of experiment descriptor object.
 */
var DATA = new Map();

var NIterationsSmall = 20;
var NIterationsBig = 20;

var ResourceTimingBufferSize = 250;

function fullSource(source, ser, exp, req) {
  return source + `?ser=${ser}&exp=${exp}&req=${req}`;
}

var xhr = new XMLHttpRequest();
function xhrRequest(src, ser, exp, req, remaining) {
  if (remaining == 0) {
    newExperiment(ser, generateRandomId(), FETCH);
    return;
  }

  xhr.onreadystatechange = () => {
    switch (xhr.readyState) {
      case XMLHttpRequest.DONE:
        let newReq = generateRandomId();
        xhrRequest(src, ser, exp, newReq, remaining - 1);
        break;
    }
  };

  xhr.open("get", fullSource(src, ser, exp, req));
  xhr.send();
}

function fetchRequest(src, ser, exp, req, remaining) {
  if (remaining == 0) {
    flushRequestData();
    finishedSeries.innerHTML += `<p>${ser}</p>`;
    return;
  }

  fetch(fullSource(src, ser, exp, req))
    .then(response => response.text())
    .then(text => {
      let newReq = generateRandomId();
      fetchRequest(src, ser, exp, newReq, remaining - 1);
    });
}

function flushRequestData() {
  const requestData = performance.getEntriesByType("resource");  

  requestData.forEach(pd => {
    if (pd.initiatorType != "xmlhttprequest" && pd.initiatorType != "fetch")
      return;
    const url = new URL(pd.name);
    const pathElements = url.pathname.split('/');
    const slug = pathElements[pathElements.length - 1];
    const query = parseQueryString(url.search);

    DATA.get(query.ser).experiments
      .get(query.exp).requests
      .set(query.req, pd.duration);
  });

  performance.clearResourceTimings();
}

function newExperiment(ser, exp, type) {
  DATA.get(ser).experiments.set(exp, {
    type: type,
    expStart: ISODateTime(),
    requests: new Map()
  });

  let src = DATA.get(ser).source;
  let iterations = DATA.get(ser).iterations;
  let req = generateRandomId();

  switch (type) {
    case XHR:
      xhrRequest(src, ser, exp, req, iterations);
      break;
    case FETCH:
      fetchRequest(src, ser, exp, req, iterations);
      break;
  }
}

function newSeries(ser, src, iterations) {
  performance.clearResourceTimings();
 
  DATA.set(ser, {
    source: src,
    iterations: iterations,
    browser: browserSpec,
    experiments: new Map()
  });

  let exp = generateRandomId();
  newExperiment(ser, exp, XHR);
}

function parseQueryString(query) {
  const q = query.charAt(0) == '?' ? query.replace('?', '') : query;
  const vars = q.split('&');
  const result = {};

  vars.forEach(el => {
    const kv = el.split('=');
    const key = decodeURIComponent(kv[0]);
    const val = decodeURIComponent(kv[1]);

    result[key] = val;
  });

  return result;
}

var downloadInfo = function() {
  var a = document.createElement("a");
  a.style = "display: none;";

  // Results will be exported as CSV, not JSON
  var exportData = function(data) {
    let result = "";

    result += "series,browser,source,iterations,experiment,"
      + "type,expStart,request,duration\n";

    data.forEach((val, key, map) => {
      const serData = {
        series: key,
        browser: val.browser,
        source: val.source,
        iterations: val.iterations
      };
      val.experiments.forEach((val, key, map) => {
        const expData = {
          experiment: key,
          type: val.type,
          expStart: val.expStart
        }; 
        val.requests.forEach((val, key, map) => {
          result += `${serData.series},${serData.browser},${serData.source},`
            + `${serData.iterations},${expData.experiment},`
            + `${expData.type},${expData.expStart},`
            + `${key},${val}\n`;
        });
      });
    });
    
    return result;
  };

  return function() {
    const blob = new Blob([exportData(DATA)], {type: "text/csv"});

    let url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = "data_" + ISODateTime().replaceAll(":", "_") + ".csv";
    a.click();
    window.URL.revokeObjectURL(url);
  }
}();

function clearResults() {
  performance.clearResourceTimings();
  DATA.forEach((val, key, map) => {
    val.experiments.forEach((val, key, map) => val.requests.clear());
    val.experiments.clear();
  });
  DATA.clear();

  finishedSeries.innerHTML = "";
}
