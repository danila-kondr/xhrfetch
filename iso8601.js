// vim: ts=2 sw=2 expandtab

function ISODate() {
  const today = new Date();
  var result = "";

  const year = today.getFullYear();
  const month = today.getMonth() + 1;
  const date = today.getDate();

  result += year; result += "-";
  if (month < 10) result += "0"; result += month; result += "-";
  if (date < 10) result += "0"; result += date;

  return result;
}

function ISODateTime() {
  const today = new Date();
  var result = "";

  const year = today.getFullYear();
  const month = today.getMonth() + 1;
  const date = today.getDate();
  const hour = today.getHours();
  const minute = today.getMinutes();
  const second = today.getSeconds();

  result += year; result += "-";
  if (month < 10) result += "0"; result += month; result += "-";
  if (date < 10) result += "0"; result += date;

  result += "T";

  if (hour < 10) result += "0"; result += hour; result += ":";
  if (minute < 10) result += "0"; result += minute; result += ":";
  if (second < 10) result += "0"; result += second;

  return result;
}
