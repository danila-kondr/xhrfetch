// vim: ts=2 sw=2 expandtab

function genRandomString() {
  var result = "";
  const alphabet = 
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (let i = 0; i < 10; i++) {
    let x = Math.floor(Math.random() * alphabet.length);
    result += alphabet[x];
  }

  return result;
}

// 0 0 0 0 0 1 1 1|1 1 2 2 2 2 2 3|3 3 3 3 4 4 4 4|4 5 5 5 5 5 6 6|6 6 6 7 7 7 7 7|
// 0 0 0 0 0|0 0 0 1 1|1 1 1 1 1|1 2 2 2 2|2 2 2 2 3|3 3 3 3 3|3 3 4 4 4|4 4 4 4 4|
//
// 1 1 1 1 1 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0| F8 00 00 00 00
// 0 0 0 0 0 1 1 1|1 1 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0| 07 C0 00 00 00
// 0 0 0 0 0 0 0 0|0 0 1 1 1 1 1 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0| 00 3E 00 00 00
// 0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 1|1 1 1 1 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0| 00 01 F0 00 00
// 0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 1 1 1 1|1 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0| 00 00 0F 80 00
// 0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 1 1 1 1 1 0 0|0 0 0 0 0 0 0 0| 00 00 00 7C 00
// 0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 1 1|1 1 1 0 0 0 0 0| 00 00 00 03 E0
// 0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 0 0 0 0 0|0 0 0 1 1 1 1 1| 00 00 00 00 1F

function generateRandomId() {
  let result = "";
  let array = new Uint8Array(10);
  const alphabet = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";

  window.crypto.getRandomValues(array);
  for (let i = 0; i < 10; i += 5) {
    let a = new Array(8);
    a[0] =  (array[i] & 0xf8) >>> 3;
    a[1] = ((array[i] & 0x07) <<  2) | ((array[i+1] & 0xc0) >>> 6);
    a[2] =  (array[i+1] & 0x3e) >>> 1;
    a[3] = ((array[i+1] & 0x01) <<  4) | ((array[i+2] & 0xf0) >>> 4);
    a[4] = ((array[i+2] & 0x0f) <<  1) | ((array[i+3] & 0x80) >>> 7);
    a[5] =  (array[i+3] & 0x7c) >>> 2;
    a[6] = ((array[i+3] & 0x03) <<  3) | ((array[i+4] & 0xe0) >>> 5);
    a[7] =  (array[i+4] & 0x1f);

    result += Array.from(a, el => alphabet[el]).join('');
  }

  return result;
}
