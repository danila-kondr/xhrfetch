// vim: ts=2 sw=2 expandtab

var browserSpec;

function genBrowserSpec() {
  const browserSpecSpan = document.getElementById("browser-spec");
  const info1 = UAParser();
  let info = info1;

  if (info.engine.is("Blink")) {
    UAParser().withClientHints().then((info) => {
      const browserString = info.browser.name + " " + info.browser.version;
      const osString = info.os.name + " " + info.os.version + " " + info.cpu.architecture;

      browserSpec = browserString + " on " + osString;
      browserSpecSpan.innerHTML = browserSpec; 
    });
    return;
  } 

  let browserString;

  // Heuristic for GNOME Web which recognized as Safari
  if (info.browser.is("safari") && info.os.is("linux"))
    browserString = "GNOME Web (like " + info.browser.name + " " + info.browser.version + ")";
  else
    browserString = info.browser.name + " " + info.browser.version;

  let osString = info.os.name;
  
  if (info.os.is("linux"))
    osString += " " + info.cpu.architecture;
  else
    osString += " " + info.os.version + " " + info.cpu.architecture;

  browserSpec = browserString + " on " + osString;
  browserSpecSpan.innerHTML = browserSpec; 
};

genBrowserSpec();
